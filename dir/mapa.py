# Importamos librerías
import branca
from branca.element import Html, IFrame
import folium
import webbrowser
import pathlib as pl


# Creación del mapa con folium
mapa = folium.Map(location=[-33.032348,-71.5719512],zoom_start=13,tiles=None)
folium.TileLayer('openstreetmap', name='Ciudades').add_to(mapa)
mapa.save('C:\\Users\\drelk\\Desktop\\Universidad\\IWG101\\Mapainteractivo\\mapa_test2.html')

# Información de los popups
html = "<p>Grúas Viña del Mar</p><p>+56950667819</p><p>https://www.necesitounagrua.cl/vina-del-mar</p><p>1 Nte. 300, Viña del Mar, Valparaíso</p><p>-33.0224353,-71.5534044</p>"
iframe1 = branca.element.IFrame(html=html, width=290, height=210)
html = "<p>Grúas Viña del Mar Autos y Motos</p><p>+56950667819</p><p>https://www.necesitounagrua.cl/vina-del-mar</p><p>2 Pte. 232-298, Viña del Mar, Valparaíso</p><p>-33.0191831,-71.5567275</p>"
iframe2 = branca.element.IFrame(html=html, width=290, height=222)
html = "<p>Servicio grúa Viña</p><p>+56981312894</p><p>5 Ote., Viña del Mar, Valparaíso</p><p>-33.0175843,-71.5454924/p>"
iframe3 = branca.element.IFrame(html=html, width=290, height=165)
html = "<p>Gruas Grossi</p><p>+56322686416</p><p>http://www.gruasgrossi.cl/</p><p>Norte 6, Valparaíso, Viña del Mar, Valparaíso</p><p>-33.0172861,-71.5574105</p>"
iframe4 = branca.element.IFrame(html=html, width=290, height=210)
html = "<p>Gruas Viña del mar</p><p>+56961222302</p><p>https://www.serviciogruas.cl/</p><p>Ecuador 85, Viña del Mar, Valparaíso</p><p>-33.0204232,-71.558012</p>"
iframe5 = branca.element.IFrame(html=html, width=290, height=210)
html = "<p>Grúas César</p><p>+56975943334</p><p>Alonso de Ercilla 636, Valparaíso, Viña del Mar, Valparaíso</p><p>-33.0274757,-71.5683117</p>"
iframe6 = branca.element.IFrame(html=html, width=290, height=170)
html = "<p>Gruas García</p><p>+56946949771</p><p>https://www.fygarriendodemaquinaria.cl/</p><p>Magallanes 13, Viña del Mar, Valparaíso</p><p>-33.0346214,-71.556382</p>"
iframe7 = branca.element.IFrame(html=html, width=280, height=205)
html = "<p>Grúas Costa</p><p>+56322216845</p><p>http://costagruas.wix.com/gruas</p><p>Pocuro 1046, Valparaíso</p><p>-33.0431646,-71.5979351</p>"
iframe8 = branca.element.IFrame(html=html, width=290, height=200)
html = "<p>Grúas viña</p><p>+56992947609</p><p>Hernan Mery 27 Valparaíso CL, Valparaíso</p><p>-33.0497336,-71.6096081</p>"
iframe9 = branca.element.IFrame(html=html, width=290, height=180)
html = "<p>Grúas Erna Limitada</p><p>+56942547913</p><p>Inglaterra 413, Viña del Mar, VS</p><p>-33.0307933,-71.5122273</p>"
iframe10 = branca.element.IFrame(html=html, width=230, height=170)
html = "<p>Grúas Campos</p><p>+56982069066</p><p>Julio Velasco 01590, Quilpué, Valparaíso</p><p>-33.0471535,-71.5028932</p>"
iframe11 = branca.element.IFrame(html=html, width=290, height=169)


# Creación de marcadores en el mapa
grua1 = folium.Marker(
    location=[-33.0221297,-71.5549102],
        popup=folium.Popup(iframe1, max_width=500),
    icon=folium.Icon(color="black")
    )
grua2 = folium.Marker(location=[-33.0191831,-71.5567275],
        popup=folium.Popup(iframe2, max_width=500),
    icon=folium.Icon(color="gray")
    )
grua3 = folium.Marker(location=[-33.0175843,-71.5454924],
        popup=folium.Popup(iframe3, max_width=500),
    icon=folium.Icon(color="blue")
    )
grua4 = folium.Marker(location=[-33.0172861,-71.5574105],
        popup=folium.Popup(iframe4, max_width=500),
    icon=folium.Icon(color="beige")
    )
grua5 = folium.Marker(location=[-33.0204232,-71.558012],
        popup=folium.Popup(iframe5, max_width=500),
    icon=folium.Icon(color="purple")
    )
grua6 = folium.Marker(location=[-33.0274757,-71.5683117],
        popup=folium.Popup(iframe6, max_width=500),
    icon=folium.Icon(color="cadetblue")
    )
grua7 = folium.Marker(location=[-33.0346214,-71.556382],
        popup=folium.Popup(iframe7, max_width=500),
    icon=folium.Icon(color="orange")
    )
grua8 = folium.Marker(location=[-33.0431646,-71.5979351],
        popup=folium.Popup(iframe8, max_width=500),
    icon=folium.Icon(color="pink")
    )
grua9 = folium.Marker(location=[-33.0497336,-71.6096081],
        popup=folium.Popup(iframe9, max_width=500),
    icon=folium.Icon(color="red")
    )
grua10 = folium.Marker(location=[-33.0307933,-71.5122273],
        popup=folium.Popup(iframe10, max_width=500),
    icon=folium.Icon(color="green")
    )
grua11 = folium.Marker(location=[-33.0471535,-71.5028932],
        popup=folium.Popup(iframe11, max_width=500),
    icon=folium.Icon(color="darkblue")
    )

# Creación de dos grupos para Valparaiso y viña del mar
grp_valpo = folium.FeatureGroup(name='Valparaíso')
grp_vina = folium.FeatureGroup(name='Viña del Mar')
grp_quilpue = folium.FeatureGroup(name='Quilpue')

# Añadimos los marcadores a los grupos
grua1.add_to(grp_vina)
grua2.add_to(grp_vina)
grua3.add_to(grp_vina)
grua4.add_to(grp_vina)
grua5.add_to(grp_vina)
grua6.add_to(grp_vina)
grua7.add_to(grp_vina)
grua8.add_to(grp_valpo)
grua9.add_to(grp_valpo)
grua10.add_to(grp_vina)
grua11.add_to(grp_quilpue)

# Añadimos los grupos al mapa
grp_valpo.add_to(mapa)
grp_vina.add_to(mapa)
grp_quilpue.add_to(mapa)

# Adicion del control de capas al mapa
folium.LayerControl().add_to(mapa)
mapa.save('C:\\Users\\drelk\\Desktop\\Universidad\\IWG101\\Mapainteractivo\\mapa_test2.html')

# Apertura del mapa creado con navegador web
mapa= 'C:\\Users\\drelk\\Desktop\\Universidad\\IWG101\\Mapainteractivo\\mapa_test2.html'
opera_path = '"C:\\Users\drelk\AppData\Local\Programs\Opera\\launcher.exe" %s'
webbrowser.get(opera_path).open(mapa)
