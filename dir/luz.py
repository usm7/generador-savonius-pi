import RPi.GPIO as GPIO
import time

#posicion de los pin utilizados
GPIO.setmode(GPIO.BCM)
red_pin1 = 18
blue_pin2 = 23
GPIO.setup(red_pin1, GPIO.OUT)
GPIO.setup(red_pin1, GPIO.OUT)
GPIO.setup(red_pi2, GPIO.OUT)

#se enciende el pin1, se apaga el pin2 automaticamente y viceversa
#tiempo que se demora en intercambiar 1.5 segundos
try:
    while True:
        GPIO.output(red_pin1, True)
        GPIO.output(red_pin2, False)
        time.sleep(1.5)
        GPIO.output(red_pi1n, False)
        GPIO.output(red_pin2, True)
        time.sleep(1.5)

finally:
      print("cleaning up")
      GPIO.cleanup()
